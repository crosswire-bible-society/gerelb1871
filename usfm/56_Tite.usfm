\id TIT
\h
\toc1 Titus
\toc2
\toc3 Titus
\c 1
\p
\v 1 Paulus, Knecht \f + \ft O. Sklave\f* Gottes, aber Apostel Jesu Christi, nach dem Glauben der Auserwählten Gottes und nach der Erkenntnis der Wahrheit, die nach der Gottseligkeit ist,
\v 2 in \f + \ft O. auf Grund\f* der Hoffnung des ewigen Lebens, welches Gott, der nicht lügen kann, verheißen hat vor den Zeiten der Zeitalter,
\v 3 zu seiner Zeit \f + \ft Eig. zu seinen Zeiten\f* aber sein Wort geoffenbart hat durch die Predigt, die mir anvertraut worden ist nach Befehl unseres Heiland-Gottes
\v 4 Titus, meinem echten Kinde nach unserem gemeinschaftlichen Glauben: Gnade und Friede von Gott, dem Vater, und Christo Jesu, unserem Heilande!
\v 5 Deswegen ließ ich dich in Kreta, daß du, was noch mangelte, in Ordnung bringen und in jeder Stadt Älteste anstellen möchtest, wie ich dir geboten hatte:
\v 6 Wenn jemand untadelig ist, eines Weibes Mann, der gläubige Kinder hat, die nicht eines ausschweifenden Lebens beschuldigt oder zügellos sind.
\v 7 Denn der Aufseher muß untadelig sein als Gottes Verwalter, nicht eigenmächtig, nicht zornmütig, nicht dem Wein ergeben, nicht ein Schläger, nicht schändlichem Gewinn nachgehend,
\v 8 sondern gastfrei, das Gute liebend, besonnen, \f + \ft O. gesunden Sinnes; so auch nachher\f* gerecht, fromm, \f + \ft O. heilig\f* enthaltsam,
\v 9 anhangend dem zuverlässigen Worte nach der Lehre, auf daß er fähig sei, sowohl mit der gesunden Lehre \f + \ft O. Belehrung\f* zu ermahnen, \f + \ft O. ermuntern\f* als auch die Widersprechenden zu überführen.
\v 10 Denn es gibt viele zügellose Schwätzer und Betrüger, besonders die aus der Beschneidung,
\v 11 denen man den Mund stopfen muß, welche ganze Häuser umkehren, indem sie um schändlichen Gewinnes willen lehren, was sich nicht geziemt. \f + \ft Eig. was man nicht soll\f*
\v 12 Es hat einer aus ihnen, ihr eigener Prophet, gesagt: "Kreter sind immer Lügner, böse, wilde Tiere, faule Bäuche".
\v 13 Dieses Zeugnis ist wahr; um dieser Ursache willen weise sie streng zurecht, \f + \ft O. überführe sie scharf\f* auf daß sie gesund seien im Glauben
\v 14 und nicht achten auf jüdische Fabeln und Gebote von Menschen, die sich von der Wahrheit abwenden.
\v 15 Den Reinen ist alles rein; den Befleckten aber und Ungläubigen ist nichts rein, sondern befleckt ist sowohl ihre Gesinnung, als auch ihr Gewissen.
\v 16 Sie geben vor, Gott zu kennen, aber in den Werken verleugnen sie ihn und sind greulich und ungehorsam und zu jedem guten Werke unbewährt.
\c 2
\p
\v 1 Du aber rede, was der gesunden Lehre \f + \ft O. Belehrung\f* geziemt:
\v 2 daß die alten Männer nüchtern seien, würdig, besonnen, gesund im Glauben, in der Liebe, im Ausharren;
\v 3 die alten Frauen desgleichen in ihrem Betragen, wie es dem heiligen Stande \f + \ft O. dem Heiligtum\f* geziemt, nicht verleumderisch, nicht Sklavinnen von vielem Wein, Lehrerinnen des Guten;
\v 4 auf daß sie die jungen Frauen unterweisen, \f + \ft O. anleiten\f* ihre Männer zu lieben, ihre Kinder zu lieben,
\v 5 besonnen, keusch, \f + \ft O. rein\f* mit häuslichen Arbeiten beschäftigt, gütig, den eigenen Männern unterwürfig zu sein, auf daß das Wort Gottes nicht verlästert werde.
\v 6 Die Jünglinge desgleichen ermahne, besonnen zu sein,
\v 7 indem du in allem dich selbst als ein Vorbild guter Werke darstellst; in der Lehre Unverderbtheit, würdigen Ernst,
\v 8 gesunde, nicht zu verurteilende Rede, auf daß der von der Gegenpartei sich schäme, indem er nichts Schlechtes über uns zu sagen hat.
\v 9 Die Knechte \f + \ft O. Sklaven\f* ermahne, ihren eigenen Herren \f + \ft Eig. Gebietern\f* unterwürfig zu sein, in allem sich wohlgefällig zu machen, \f + \ft W. wohlgefällig zu sein\f* nicht widersprechend,
\v 10 nichts unterschlagend, sondern alle gute Treue erweisend, auf daß sie die Lehre, die unseres Heiland-Gottes ist, zieren in allem.
\v 11 Denn die Gnade Gottes ist erschienen, heilbringend für alle Menschen, \f + \ft O. Die heilbringende Gnade Gottes ist erschienen allen Menschen\f*
\v 12 und unterweist uns, auf daß wir, die Gottlosigkeit und die weltlichen Lüste verleugnend, besonnen und gerecht und gottselig leben in dem jetzigen Zeitlauf,
\v 13 indem wir erwarten die glückselige Hoffnung und Erscheinung der Herrlichkeit unseres großen Gottes und Heilandes Jesus Christus,
\v 14 der sich selbst für uns gegeben hat, auf daß er uns loskaufte von aller Gesetzlosigkeit und reinigte sich selbst ein Eigentumsvolk, eifrig in guten Werken.
\v 15 Dieses rede und ermahne und überführe mit aller Machtvollkommenheit. Laß dich niemand verachten.
\c 3
\p
\v 1 Erinnere sie, Obrigkeiten und Gewalten untertan zu sein, Gehorsam zu leisten, zu jedem guten Werke bereit zu sein;
\v 2 niemand zu lästern, nicht streitsüchtig zu sein, gelinde, alle Sanftmut erweisend gegen alle Menschen.
\v 3 Denn einst waren auch wir unverständig, ungehorsam, irregehend, dienten mancherlei Lüsten und Vergnügungen, führten unser Leben in Bosheit und Neid, verhaßt und einander hassend.
\v 4 Als aber die Güte und die Menschenliebe unseres Heiland-Gottes erschien,
\v 5 errettete er uns, nicht aus \f + \ft O. auf dem Grundsatz von\f* Werken, die, in Gerechtigkeit vollbracht, wir getan hatten, sondern nach seiner Barmherzigkeit durch die Waschung der Wiedergeburt und Erneuerung des Heiligen Geistes,
\v 6 welchen er reichlich über uns ausgegossen hat durch Jesum Christum, unseren Heiland,
\v 7 auf daß wir, gerechtfertigt durch seine Gnade, Erben würden nach der Hoffnung des ewigen Lebens. \f + \ft O. der Hoffnung nach Erben des ewigen Lebens würden\f*
\v 8 Das Wort ist gewiß; \f + \ft O. zuverlässig, treu\f* und ich will, daß du auf diesen Dingen fest bestehst, auf daß die, welche Gott geglaubt haben, Sorge tragen, gute Werke zu betreiben. Dies ist gut und nützlich für die Menschen.
\v 9 Törichte Streitfragen aber und Geschlechtsregister und Zänkereien und Streitigkeiten über das Gesetz vermeide, denn sie sind unnütz und eitel.
\v 10 Einen sektiererischen Menschen weise ab nach einer ein- und zweimaligen Zurechtweisung,
\v 11 da du weißt, daß ein solcher verkehrt ist und sündigt, indem er durch sich selbst verurteilt ist.
\v 12 Wenn ich Artemas oder Tychikus zu dir senden werde, so befleißige dich, zu mir nach Nikopolis zu kommen, denn ich habe beschlossen, daselbst zu überwintern.
\v 13 Zenas, dem Gesetzgelehrten, und Apollos gib mit Sorgfalt das Geleit, \f + \ft O. rüste mit Sorgfalt für die Reise aus\f* auf daß ihnen nichts mangle.
\v 14 Laß aber auch die Unsrigen lernen, für die notwendigen Bedürfnisse gute Werke zu betreiben, auf daß sie nicht unfruchtbar seien.
\v 15 Es grüßen dich alle, die bei mir sind. Grüße, die uns lieben im Glauben. Die Gnade sei mit euch allen!
