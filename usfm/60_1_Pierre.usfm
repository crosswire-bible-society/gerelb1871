\id 1PE
\h
\toc1 1 Petrus
\toc2
\toc3 1. Petr
\c 1
\p
\v 1 Petrus, Apostel Jesu Christi, den Fremdlingen \f + \ft O. denen, die ohne Bürgerrecht sind, oder den Beisassen; wie \fr Kap 2,11\f* von der Zerstreuung von Pontus, Galatien, Kappadocien, Asien und Bithynien, auserwählt
\v 2 nach Vorkenntnis Gottes, des Vaters, durch \f + \ft O. in\f* Heiligung \f + \ft S. die Anm. zu 2. Thess 2,13\f* des Geistes, zum Gehorsam und zur Blutbesprengung Jesu Christi: Gnade und Friede sei euch \f + \ft W. Gnade euch und Friede sei\f* vermehrt!
\v 3 Gepriesen sei der Gott und Vater unseres Herrn Jesus Christus, der nach seiner großen Barmherzigkeit uns wiedergezeugt \f + \ft O. wiedergeboren\f* hat zu einer lebendigen Hoffnung durch die Auferstehung Jesu Christi aus den Toten,
\v 4 zu einem unverweslichen und unbefleckten und unverwelklichen Erbteil, welches in den Himmeln aufbewahrt ist für euch,
\v 5 die ihr durch \f + \ft Eig. in, d. i. infolge, kraft\f* Gottes Macht durch Glauben bewahrt werdet zur Errettung, \f + \ft O. Seligkeit; so auch nachher\f* die bereit ist, in der letzten Zeit geoffenbart zu werden;
\v 6 worin \f + \ft O. in welcher (d. i. Zeit)\f* ihr frohlocket, die ihr jetzt eine kleine Zeit, wenn es nötig ist, betrübt seid durch mancherlei Versuchungen; \f + \ft O. Prüfungen\f*
\v 7 auf daß die Bewährung \f + \ft O. Erprobung\f* eures Glaubens, viel köstlicher als die des Goldes, das vergeht, aber durch Feuer erprobt wird, erfunden werde zu Lob und Herrlichkeit und Ehre in der Offenbarung Jesu Christi;
\v 8 welchen ihr, obgleich ihr ihn nicht gesehen habt, liebet; an welchen glaubend, obgleich ihr ihn jetzt nicht sehet, ihr mit unaussprechlicher und verherrlichter Freude frohlocket,
\v 9 indem ihr das Ende eures Glaubens, die Errettung der Seelen, \f + \ft Eig. Seelen-Errettung, im Gegensatz zu leiblichen und zeitlichen Befreiungen\f* davontraget;
\v 10 über welche Errettung Propheten nachsuchten und nachforschten, die von der Gnade gegen euch geweissagt haben,
\v 11 forschend, auf welche oder welcherlei Zeit der Geist Christi, der in ihnen war, hindeutete, als er von den Leiden, die auf Christum kommen sollten, und von den Herrlichkeiten danach zuvor zeugte;
\v 12 welchen es geoffenbart wurde, daß sie nicht für sich selbst, sondern für euch die Dinge bedienten, die euch jetzt verkündigt worden sind durch die, welche euch das Evangelium gepredigt haben durch \f + \ft W. in, d. h. in der Kraft des\f* den vom Himmel gesandten Heiligen Geist, in welche Dinge Engel hineinzuschauen begehren.
\v 13 Deshalb umgürtet die Lenden eurer Gesinnung, seid nüchtern und hoffet \f + \ft Eig. Die Lenden umgürtet habend, nüchtern seiend, hoffet\f* völlig auf die Gnade, die euch gebracht wird bei der Offenbarung Jesu Christi;
\v 14 als Kinder des Gehorsams bildet euch nicht \f + \ft O. die ihr als nicht gebildet seid\f* nach den vorigen Lüsten in eurer Unwissenheit,
\v 15 sondern wie der, welcher euch berufen hat, heilig ist, seid auch ihr heilig in allem Wandel;
\v 16 denn es steht geschrieben: "Seid heilig, denn ich bin heilig". \x + \xt 3. Mose 11,45\x*
\v 17 Und wenn ihr den als Vater anrufet, der ohne Ansehen der Person richtet nach eines jeden Werk, so wandelt die Zeit eurer Fremdlingschaft in Furcht,
\v 18 indem ihr wisset, daß ihr nicht mit verweslichen Dingen, mit Silber oder Gold, erlöst worden seid von eurem eitlen, von den Vätern überlieferten Wandel,
\v 19 sondern mit dem kostbaren Blute Christi, als eines Lammes ohne Fehl und ohne Flecken;
\v 20 welcher zwar zuvorerkannt ist vor Grundlegung der Welt, aber geoffenbart worden am Ende der Zeiten um euretwillen,
\v 21 die ihr durch ihn glaubet \f + \ft O. nach and. Les.: gläubig seid\f* an Gott, der ihn aus den Toten auferweckt und ihm Herrlichkeit gegeben hat, auf daß euer Glaube und eure Hoffnung auf Gott sei. \f + \ft O. so daß ist\f*
\v 22 Da ihr eure Seelen gereinigt habt durch den Gehorsam gegen die Wahrheit zur ungeheuchelten Bruderliebe, so liebet einander mit Inbrunst \f + \ft O. anhaltend, beharrlich\f* aus reinem Herzen,
\v 23 die ihr nicht wiedergeboren \f + \ft O. wiedergezeugt\f* seid aus verweslichem Samen, sondern aus unverweslichem, durch das lebendige und bleibende Wort Gottes;
\v 24 denn "alles Fleisch ist wie Gras, und alle seine Herrlichkeit wie des Grases Blume. Das Gras ist verdorrt, und seine Blume ist abgefallen;
\v 25 aber das Wort des Herrn bleibt in Ewigkeit." \x + \xt Jes 40,6-8\x* Dies aber ist das Wort, welches euch verkündigt \f + \ft W. evangelisiert\f* worden ist.
\c 2
\p
\v 1 Leget nun ab alle Bosheit und allen Trug und Heuchelei und Neid \f + \ft Eig. Heucheleien und Neidereien\f* und alles üble Nachreden,
\v 2 und wie \f + \ft O. als\f* neugeborene Kindlein seid begierig \f + \ft Eig. abgelegt habend, seid begierig\f* nach der vernünftigen, \f + \ft Da der griechische Ausdruck von logos(= Wort) abgeleitet ist, so üb. and.: vom Worte herstammend, wortgemäß; oder um die wahrscheinliche Anspielung auf das Wort "logos" anzudeuten: vernünftige unverfälschte Mich des Wortes\f* unverfälschten Milch, auf daß ihr durch dieselbe wachset zur Errettung,
\v 3 wenn ihr anders geschmeckt habt, daß der Herr gütig ist.
\v 4 Zu welchem kommend, als zu einem lebendigen Stein, von Menschen zwar verworfen, bei Gott aber auserwählt, kostbar,
\v 5 seid \f + \ft werdet\f* auch ihr selbst als lebendige Steine, aufgebaut, ein geistliches Haus, ein heiliges Priestertum, um darzubringen geistliche Schlachtopfer, Gott wohlannehmlich durch Jesum Christum.
\v 6 Denn es ist in der Schrift enthalten: "Siehe, ich lege in Zion einen Eckstein, einen auserwählten, kostbaren; und wer an ihn glaubt, \f + \ft O. auf ihn vertraut\f* wird nicht zu Schanden werden." \x + \xt Jes 28,16\x*
\v 7 Euch nun, die ihr glaubet, ist die Kostbarkeit; den Ungehorsamen \f + \ft O. Ungläubigen\f* aber: "Der Stein, den die Bauleute verworfen haben, dieser ist zum Eckstein \f + \ft W. Haupt der Ecke; Ps 118,22\f* geworden",
\v 8 und "ein Stein des Anstoßes und ein Fels des Ärgernisses", \x + \xt Jes 8,14\x* die sich, da sie nicht gehorsam sind, an dem Worte stoßen, \f + \ft O. die sich, da sie dem Worte nicht gehorchen, stoßen\f* wozu sie auch gesetzt worden sind.
\v 9 Ihr aber seid ein auserwähltes Geschlecht, ein königliches Priestertum, eine heilige Nation, ein Volk zum Besitztum, \f + \ft Vergl. 2. Mose 19,5. 6.\f* damit ihr die Tugenden \f + \ft O. Vortrefflichkeiten\f* dessen verkündigt, der euch berufen hat aus der Finsternis zu seinem wunderbaren Licht;
\v 10 die ihr einst "nicht ein Volk" waret, jetzt aber ein Volk Gottes seid; die ihr "nicht Barmherzigkeit empfangen hattet", jetzt aber Barmherzigkeit empfangen habt. \f + \ft Vergl. Hos 1,10;2,23\f*
\v 11 Geliebte, ich ermahne euch als Fremdlinge und als die ihr ohne Bürgerrecht seid, \f + \ft O. und als Beisassen\f* daß ihr euch enthaltet von den fleischlichen Lüsten, welche wider die Seele streiten,
\v 12 indem ihr euren Wandel unter den Nationen ehrbar führet, auf daß sie, worin sie wider euch als Übeltäter reden, aus den guten Werken, die sie anschauen, Gott verherrlichen am Tage der Heimsuchung.
\v 13 Unterwerfet euch nun aller menschlichen Einrichtung um des Herrn willen: es sei dem Könige als Oberherrn,
\v 14 oder den Statthaltern als denen, die von ihm gesandt werden zur Bestrafung der Übeltäter, aber zum Lobe derer, die Gutes tun.
\v 15 Denn also ist es der Wille Gottes, daß ihr durch Gutestun die Unwissenheit der unverständigen Menschen zum Schweigen bringet:
\v 16 als Freie, und die nicht die Freiheit zum Deckmantel der Bosheit haben, sondern als Knechte \f + \ft O. Sklaven\f* Gottes.
\v 17 Erweiset allen Ehre; liebet die Brüderschaft; fürchtet Gott; ehret den König.
\v 18 Ihr Hausknechte, seid den Herren \f + \ft Eig. Gebietern\f* unterwürfig in aller Furcht, nicht allein den guten und gelinden, sondern auch den verkehrten.
\v 19 Denn dies ist wohlgefällig, wenn jemand um des Gewissens vor Gott \f + \ft O. Gott gegenüber\f* willen Beschwerden erträgt, indem er ungerecht leidet.
\v 20 Denn was für ein Ruhm ist es, wenn ihr ausharret, indem ihr sündiget und geschlagen werdet? Wenn ihr aber ausharret, indem ihr Gutes tut und leidet, das ist wohlgefällig bei Gott.
\v 21 Denn hierzu seid ihr berufen worden; denn auch Christus hat für euch gelitten, euch ein Beispiel \f + \ft O. Vorbild\f* hinterlassend, auf daß ihr seinen Fußstapfen nachfolget;
\v 22 welcher keine Sünde tat, noch wurde Trug in seinem Munde erfunden, \f + \ft Vergl. \fr Jes 53,9\f*
\v 23 der, gescholten, nicht wiederschalt, leidend, nicht drohte, sondern sich \f + \ft O. es\f* dem übergab, der recht richtet;
\v 24 welcher selbst unsere Sünden an seinem Leibe auf dem Holze \f + \ft O. auf das Holz\f* getragen hat, auf daß wir, den Sünden abgestorben, der Gerechtigkeit leben, durch dessen Striemen \f + \ft O. Wunden\f* ihr heil geworden seid. \x + \xt Jes 53,5\x*
\v 25 Denn ihr ginget in der Irre wie Schafe, aber ihr seid jetzt zurückgekehrt zu dem Hirten und Aufseher eurer Seelen.
\c 3
\p
\v 1 Gleicherweise ihr Weiber, seid euren eigenen Männern unterwürfig, auf daß, wenn auch etliche dem Worte nicht gehorchen, sie durch den Wandel \f + \ft O. das Verhalten; so auch v 16\f* der Weiber ohne Wort mögen gewonnen werden,
\v 2 indem sie euren in Furcht keuschen Wandel angeschaut haben;
\v 3 deren Schmuck sei nicht der auswendige durch Flechten der Haare und Umhängen von Gold oder Anziehen von Kleidern,
\v 4 sondern der verborgene Mensch des Herzens in dem unverweslichen Schmuck des sanften und stillen Geistes, welcher vor Gott sehr köstlich ist.
\v 5 Denn also schmückten sich auch einst die heiligen Weiber, die ihre Hoffnung auf Gott setzten, indem sie ihren eigenen Männern unterwürfig waren:
\v 6 wie Sarah dem Abraham gehorchte und ihn Herr nannte, deren Kinder ihr geworden seid, wenn \f + \ft O. indem\f* ihr Gutes tut und keinerlei Schrecken fürchtet.
\v 7 Ihr Männer gleicherweise, wohnet bei ihnen nach Erkenntnis, \f + \ft O. mit Einsicht\f* als bei einem schwächeren Gefäße, dem weiblichen, ihnen Ehre gebend, als die auch Miterben der Gnade des Lebens sind, auf daß eure Gebete nicht verhindert werden.
\v 8 Endlich aber seid alle gleichgesinnt, mitleidig, voll brüderlicher Liebe, barmherzig, demütig, \f + \ft O. niedriggesinnt\f*
\v 9 und vergeltet nicht Böses mit Bösem, oder Scheltwort mit Scheltwort, sondern im Gegenteil segnet, weil ihr dazu berufen worden seid, daß ihr Segen ererbet.
\v 10 "Denn wer das Leben lieben und gute Tage sehen will, der enthalte seine Zunge vom Bösen, und seine Lippen, daß sie nicht Trug reden;
\v 11 er wende sich ab vom Bösen und tue Gutes; er suche Frieden und jage ihm nach;
\v 12 denn die Augen des Herrn sind gerichtet auf die Gerechten, und seine Ohren auf ihr Flehen; das Angesicht des Herrn aber ist wider die, welche Böses tun." \x + \xt Ps 34,12-16\x*
\v 13 Und wer ist, der euch Böses tun wird, wenn ihr Nachahmer des Guten geworden seid?
\v 14 Aber wenn ihr auch leiden solltet um der Gerechtigkeit willen, glückselig seid ihr! Fürchtet aber nicht ihre Furcht, noch seid bestürzt,
\v 15 sondern heiliget Christus, den Herrn, \f + \ft Eig. den Herrn, den Christus\f* in euren Herzen. \f + \ft Vergl. Jes 8,12. 13.\f* Seid aber jederzeit bereit zur Verantwortung gegen jeden, der Rechenschaft von euch fordert über die Hoffnung, die in euch ist, aber mit Sanftmut und Furcht;
\v 16 indem ihr ein gutes Gewissen habt, auf daß, worin sie wider euch als Übeltäter reden, die zu Schanden werden, welche euren guten Wandel in Christo verleumden.
\v 17 Denn es ist besser, wenn der Wille Gottes es will, \f + \ft Eig. wollen sollte\f* für Gutestun zu leiden, als für Bösestun.
\v 18 Denn es hat ja \f + \ft W. auch\f* Christus einmal für Sünden gelitten, der Gerechte für die Ungerechten, auf daß er uns zu Gott führe, getötet nach \f + \ft O. in\f* dem Fleische, aber lebendig gemacht nach dem Geiste,
\v 19 in welchem er auch hinging und predigte den Geistern, die im Gefängnis sind,
\v 20 welche einst ungehorsam waren, \f + \ft O. nicht glaubten\f* als die Langmut Gottes harrte in den Tagen Noahs, während die Arche zugerichtet wurde, in welche \f + \ft O. in welche eingehend\f* wenige, daß ist acht Seelen, durch Wasser \f + \ft O. durch Wasser hindurch\f* gerettet wurden,
\v 21 welches Gegenbild auch euch jetzt errettet, das ist die Taufe \f + \ft nicht ein Ablegen der Unreinigkeit des Fleisches, sondern das Begehren (O. die Forderung, das Zeugnis) eines guten Gewissens vor (Eig. zu, an) Gott\f*, durch die Auferstehung Jesu Christi,
\v 22 welcher, in den Himmel gegangen, zur Rechten Gottes ist, indem Engel und Gewalten und Mächte ihm unterworfen sind.
\c 4
\p
\v 1 Da nun Christus für uns im Fleische gelitten hat, so waffnet auch ihr euch mit demselben Sinne; denn wer \f + \ft O. Sinne: daß wer usw.\f* im Fleische gelitten hat, ruht von der Sünde \f + \ft O. hat abgeschlossen, ist fertig mit der Sünde,\f*
\v 2 um die im Fleische noch übrige Zeit nicht mehr den Lüsten der Menschen, sondern dem Willen Gottes zu leben.
\v 3 Denn die vergangene Zeit ist uns genug, den Willen der Nationen vollbracht zu haben, indem wir wandelten in Ausschweifungen, Lüsten, Trunkenheit, Festgelagen, Trinkgelagen und frevelhaften Götzendienereien;
\v 4 wobei es sie befremdet, daß ihr nicht mitlaufet zu demselben Treiben \f + \ft O. Überströmen\f* der Ausschweifung, und lästern euch,
\v 5 welche dem Rechenschaft geben werden, der bereit ist, Lebendige und Tote zu richten.
\v 6 Denn dazu ist auch den Toten gute Botschaft verkündigt worden, auf daß sie gerichtet werden möchten dem Menschen gemäß nach \f + \ft O. in\f* dem Fleische, aber leben möchten Gott gemäß nach \f + \ft O. in\f* dem Geiste.
\v 7 Es ist aber nahe gekommen das Ende aller Dinge. Seid nun besonnen und seid nüchtern zum Gebet. \f + \ft Eig. zu den Gebeten\f*
\v 8 Vor allen Dingen aber habt untereinander eine inbrünstige Liebe, denn die Liebe bedeckt eine Menge von Sünden.
\v 9 Seid gastfrei gegeneinander ohne Murren;
\v 10 je nachdem ein jeder eine Gnadengabe empfangen hat, dienet einander damit als gute Verwalter der mancherlei Gnade Gottes.
\v 11 Wenn jemand redet, so rede er als Aussprüche Gottes; wenn jemand dient, so sei es als aus der Kraft, die Gott darreicht, auf daß in allem Gott verherrlicht werde durch Jesum Christum, welchem die Herrlichkeit ist und die Macht von Ewigkeit zu Ewigkeit. \f + \ft W. in die Zeitalter der Zeitalter; so auch \fr Kap 5,11\f* Amen.
\v 12 Geliebte, laßt euch das Feuer der Verfolgung unter euch, das euch zur Versuchung \f + \ft O. Prüfung\f* geschieht, nicht befremden, als begegne euch etwas Fremdes;
\v 13 sondern insoweit ihr der Leiden des Christus teilhaftig seid, freuet euch, auf daß ihr auch in der Offenbarung seiner Herrlichkeit mit Frohlocken euch freuet.
\v 14 Wenn ihr im Namen Christi geschmäht werdet, glückselig seid ihr! denn der Geist der Herrlichkeit und der Geist Gottes \f + \ft O. der Geist der Herrlichkeit und Gottes\f* ruht auf euch. Bei ihnen freilich wird er verlästert, bei euch aber wird er verherrlicht.
\v 15 Daß doch niemand von euch leide als Mörder oder Dieb oder Übeltäter, oder als einer, der sich in fremde Sachen mischt;
\v 16 wenn aber als Christ, so schäme er sich nicht, sondern verherrliche Gott in diesem Namen.
\v 17 Denn die Zeit ist gekommen, daß das Gericht anfange bei \f + \ft W. von an\f* dem Hause Gottes; wenn aber zuerst bei \f + \ft W. von an\f* uns, was wird das Ende derer sein, die dem Evangelium Gottes nicht gehorchen! \f + \ft O. glauben\f*
\v 18 Und wenn der Gerechte mit Not errettet wird, wo will der Gottlose und Sünder erscheinen?
\v 19 Daher sollen auch die, welche nach dem Willen Gottes leiden, einem treuen Schöpfer ihre Seelen befehlen im Gutestun.
\c 5
\p
\v 1 Die Ältesten, die unter euch sind, ermahne ich, der Mitälteste und Zeuge der Leiden des Christus und auch Teilhaber der Herrlichkeit, die geoffenbart werden soll: \f + \ft O. im Begriff steht, geoffenbart zu werden\f*
\v 2 Hütet die Herde Gottes, die bei euch \f + \ft O. unter euch, wie v 1\f* ist, indem ihr die Aufsicht nicht aus Zwang führet, sondern freiwillig, auch nicht um schändlichen Gewinn, sondern bereitwillig,
\v 3 nicht als die da herrschen über ihre \f + \ft W. die\f* Besitztümer, \f + \ft O. ihr Erbteil; eig. das durchs Los Zugefallene\f* sondern indem ihr Vorbilder der Herde seid.
\v 4 Und wenn der Erzhirte offenbar geworden ist, so werdet ihr die unverwelkliche Krone der Herrlichkeit empfangen.
\v 5 Gleicherweise ihr Jüngeren, seid den Ältesten untertan. Alle aber seid gegeneinander mit Demut fest umhüllt; denn "Gott widersteht den Hochmütigen, den Demütigen aber gibt er Gnade". \x + \xt Spr 3,34\x*
\v 6 So demütiget euch nun unter die mächtige Hand Gottes, auf daß er euch erhöhe zur rechten Zeit,
\v 7 indem ihr alle eure Sorge auf ihn werfet; \f + \ft Eig. geworfen habt\f* denn er ist besorgt für euch. \f + \ft O. ihm liegt an euch\f*
\v 8 Seid nüchtern, wachet; euer Widersacher, der Teufel, geht umher wie ein brüllender Löwe und sucht, wen er verschlinge.
\v 9 Dem widerstehet standhaft im \f + \ft O. durch\f* Glauben, da ihr wisset, daß dieselben Leiden sich vollziehen an eurer Brüderschaft, die in der Welt ist.
\v 10 Der Gott aller Gnade aber, der euch berufen hat zu seiner ewigen Herrlichkeit in Christo Jesu, nachdem ihr eine kleine Zeit gelitten habt, er selbst wird euch vollkommen machen, \f + \ft O. vollenden, alles Mangelnde ersetzen\f* befestigen, kräftigen, gründen.
\v 11 Ihm sei die Herrlichkeit und die Macht von Ewigkeit zu Ewigkeit! Amen.
\v 12 Durch Silvanus, \f + \ft d. i. Silas\f* den treuen Bruder, wie ich dafür halte, habe ich euch mit wenigem \f + \ft O. den euch treuen Bruder, habe ich mit wenigem\f* geschrieben, euch ermahnend \f + \ft O. ermunternd\f* und bezeugend, daß dies die wahre Gnade Gottes ist, in welcher ihr stehet.
\v 13 Es grüßt euch die Miterwählte in Babylon und Markus, mein Sohn.
\v 14 Grüßet einander mit dem Kuß der Liebe. Friede euch allen, die ihr in Christo seid!
